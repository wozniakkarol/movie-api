## How to import project in Intellij IDEA
#### Open project in Intellij IDEA
Open -> choose project directory -> Open

Check options:

* Use auto-import

* Create separate module per source set

* Use default gradle wrapper

#### Needed plugins

* Lombok 

#### Set annotation processing
Open Settings/Preferences -> Build, Execution, Deployment -> Compiler -> Annotation Processors

Check options:

* Enable annotation processing

* Obtain processors from project class path

* Module content root

Set source directories paths to:

* Production sources directory: ```/```

* Test sources directory: ```/```

#### Enable Lombok
1. Open Settings/Preferences -> Other settings -> Lombok plugin
2. Check option: Enable Lombok plugin for this project

# Docker 

The project contains docker-compose with two services:

* kafka


